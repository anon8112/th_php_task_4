<?php

class NoteCtrl {
	
	// Db Connection
	private $con;

	function __construct($con) {
		$this->con = $con;
		
		$sql = 'CREATE TABLE IF NOT EXISTS Notes(
				id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				content VARCHAR(70) NOT NULL)';
		
		$this->con->query($sql);
	}

	public function get() {
		$sql = "SELECT * FROM Notes";
		$result = $this->con->query($sql);
		
		if($result->num_rows > 0) {
			return new Response(true, '', result_to_array($result));
		}
		return new Response(false, 'No Notes found');
	}

	public function add($data) {
		$sql = "INSERT INTO Notes (content) VALUES ('". $data ."')";
		
		if($this->con->query($sql) === TRUE) {
			return new Response(true, 'Note added successfully');
		}
		return new Response(false, $this->con->error);
	}

	public function edit($data) {
		$sql = "UPDATE Notes SET content='". $data->{'content'} . "'
				WHERE id=" . $data->{'id'};
		
		if($this->con->query($sql) === TRUE) {
			return new Response(true, 'Note updated successfully');
		}
		return new Response(false, $this->con->error);
	}

	public function remove($id) {
		$sql = 'DELETE FROM Notes WHERE id="' . $id . '"';
		
		if($this->con->query($sql) === TRUE) {
			return new Response(true,'Note removed');
		}
		return new Response(false, $this->con->error);
	}

}

?>
