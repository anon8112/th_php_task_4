<?php

class UserCtrl {
	
	// Db Connection
	private $con;

	function __construct($con) {
		$this->con = $con;
		
		$sql = 'CREATE TABLE IF NOT EXISTS Users(
				f_name VARCHAR(20) NOT NULL,
				s_name VARCHAR(30) NOT NULL,
				university VARCHAR(25) NOT NULL,
				img_url VARCHAR(50) NOT NULL
				)';
		
		$this->con->query($sql);
	}

	public function get() {
		$sql = "SELECT * FROM Users";
		$result = $this->con->query($sql);
		
		if($result->num_rows > 0) {
			return new Response(true, '', mysqli_fetch_array($result));
		}
		return new Response(false,'No User found');
	}

	public function set($data) {
		$sql = 'INSERT INTO Users SET '. 
			'f_name="' . $data->{'f_name'} . '", '.
			's_name="' . $data->{'s_name'} . '", '.
			'university="' . $data->{'university'} . '"';
		
		if($this->con->query($sql) === TRUE) {
			return new Response(true, 'User set successfull');
		}
		return new Response(false, $this->con->error);
	}

	public function edit($data) {
		$sql = 'UPDATE Users SET '. 
			'f_name="' . $data->{'f_name'} . '", '.
			's_name="' . $data->{'s_name'} . '", '.
			'university="' . $data->{'university'} . '"';
		
		if($this->con->query($sql) === TRUE) {
			return new Response(true, 'User edit successfull');
		}
		return new Response(false, $this->con->error);
	}

	public function upload_img() {
		
	}

}


?>
