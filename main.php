<?php

include 'util.php';
include 'NoteCtrl.php';
include 'UserCtrl.php';

// JSON Rest Api um Server und Client seperat zu halten
// Html Templates werden vom Client verwaltet
// Der Server dient zum Datenausstausch/Datenverwaltung
// Ermoeglicht mehrere Frontends mit gleichem Backend


// Response
$res = new Response(false, 'db error');

// Request
$req = new Request();
$req->url_ext = $_GET["action"];
$req->data = json_decode(file_get_contents('php://input'));

$con = mysqli_connect('localhost', 'root', '', 'php_4');
if(!$con) exit(json_encode($res));

// NoteController
$note = new NoteCtrl($con);
// UserController
$user = new UserCtrl($con);

// Routes
switch($req->url_ext) {
	case "get_user":
		$res = $user->get();
		break;
	case "set_user":
		$res = $user->set($req->data);
		break;
	case "edit_user":
		$res = $user->edit($req->data);
		break;
	case "upload_img":
		$res = $user->upload_img($data);
		break; 
	case "get_notes":
		$res = $note->get();
		break;
	case "add_note":
		$res = $note->add($req->data->{'content'});
		break;
	case "edit_note":
		$res = $note->edit($req->data);
		break;
	case "remove_note":
		$res = $note->remove($req->data->{'id'});
		break;
	default:
		$res->msg = "request not found";
}




exit(json_encode($res));
?>
