<?php
// Utilities: Helper Functions / Klassen

class Request {
	public $url_ext;
	public $data;
	public $msg;
}

class Response {
	public $valid;
	public $msg;
	public $data;
	function __construct($valid, $msg, $data = '') {
		$this->valid = $valid;
		$this->msg = $msg;
		$this->data = $data;
	}
}


function result_to_array($result) {
	$arr = [];

	while($row = mysqli_fetch_assoc($result)) {
			$arr[] = $row;
	}
	return $arr;
}

?>
